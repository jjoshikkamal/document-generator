/**
 * Created by kamal2179 on 8/28/2015.
 */
var fs = require('fs');
var Docxtemplater = require('docxtemplater'),
ImageModule=require("docxtemplater-image-module");

imageModule=new ImageModule({centered:false});
//Load the docx file as a binary
var content = fs
    .readFileSync( "input-overview.docx", "binary");

var doc = new Docxtemplater(content);
var tbl = '<w:p><w:pPr><w:rPr>' +
    '<w:color w:val="FF0000"/></w:rPr>' +
    '</w:pPr><w:r><w:rPr><w:color w:val="FF0000"/></w:rPr><w:t>My custom</w:t></w:r><w:r><w:rPr><w:color w:val="00FF00"/></w:rPr><w:t>XML</w:t></w:r></w:p>';
doc.attachModule(imageModule);
//set the templateVariables
doc.setData({
    date: "28-08-2015",
    googleMaps: "../Images/map.png",
    chartBar: "../Images/charts-bar.png",
    chartLine: "../Images/charts-line.png",
    officeSpace: "Holborn - Office space",
    tbl: tbl,
    "takeups":
    [
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
        {"sector":"Associations",sqft:"10,000", avg:"81"},
        {"sector":"Property",sqft:"1,410,000", avg:"91"},
        {"sector":"Government",sqft:"61,000", avg:"33"},
        {"sector":"Financial",sqft:"80,000", avg:"66"},
    ]
});

//apply them (replace all occurences of {first_name} by Hipp, ...)
doc.render();

var buf = doc.getZip()
    .generate({type:"nodebuffer"});

fs.writeFileSync(__dirname+"/output.docx",buf);