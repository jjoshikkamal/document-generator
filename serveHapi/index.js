/**
 * Created by osboxes on 8/7/15.
 */
var path = require("path"),
    hapi = require("hapi"),
    handlebars = require("handlebars"),
    charts = require("./data.js").charts,
    fs = require("fs");

var viewModel = {title: "Hello world", body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    charts: charts };
var server = new hapi.Server();
handlebars.registerHelper('json', function(obj) {
    return JSON.stringify(obj);
});

server.connection({
    host: "127.0.0.1",
    port: 8000
});


server.views({
    engines: {
        html: handlebars
    },
    path: path.join("../","templates")
});

server.route({
    method: "GET",
    path: "/exportdocx",
    handler: function (req, resp){
        var data = {
            date: "28-08-2015",
            googleMaps: "../Images/map.png",
            chartBar: "../Images/charts-bar.png",
            chartLine: "../Images/charts-line.png",
            officeSpace: "Holborn - Office space",
            "takeups": [
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
            {"sector": "Associations", sqft: "10,000", avg: "81"},
            {"sector": "Property", sqft: "1,410,000", avg: "91"},
            {"sector": "Government", sqft: "61,000", avg: "33"},
            {"sector": "Financial", sqft: "80,000", avg: "66"},
        ]
        };
        var docX = require("../docxTemplater/docxTemplater");
        console.log(docX);
        var buff = docX.docxTemplater("../docxTemplater/input-overview.docx",data);
        resp(buff)
            .header('Content-disposition', 'attachment; filename=export.docx')
            //.header('Content-Type','application/vnd.openxmlformats-officedocument.wordprocessingml.document');
    }
});
server.route({
    method: "GET",
    path: "/templatepage",
    handler: function (req, resp){

        resp.view("templatepage", viewModel)
    }
});

server.route({
        method: "GET",
        path: "/lib/{param*}",
        handler: {
            directory: {
                path: "../lib",
                listing: true
            }
        }

    }
);

server.route({
        method: "GET",
        path: "/Images/{param*}",
        handler: {
            directory: {
                path: "../Images",
                listing: true
            }
        }

    }
);




server.start();
