/**
 * Created by kamal2179 on 8/30/2015.
 */
exports.docxTemplater = function(inputFile, data) {
//Load the docx file as a binary
    var fs = require('fs');
console.log("hello")
    var Docxtemplater = require('docxtemplater'),
        ImageModule=require("docxtemplater-image-module");

    imageModule=new ImageModule({centered:false});

    var content = fs
        .readFileSync(inputFile, "binary");

    var doc = new Docxtemplater(content);

    doc.attachModule(imageModule);
//set the templateVariables
    var tbl = '<w:p><w:pPr><w:rPr>' +
        '<w:color w:val="FF0000"/></w:rPr>' +
        '</w:pPr><w:r><w:rPr><w:color w:val="FF0000"/></w:rPr><w:t>My custom</w:t></w:r><w:r><w:rPr><w:color w:val="00FF00"/></w:rPr><w:t>XML</w:t></w:r></w:p>';
    doc.setData(data);

//apply them (replace all occurences of {first_name} by Hipp, ...)
    doc.render();

    var buf = doc.getZip()
        .generate({type: "nodebuffer"});

    //fs.writeFileSync(__dirname + "/output.docx", buf);
    return buf;

}


