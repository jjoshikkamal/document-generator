exports.charts ={
  "options": {

    ///Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines : true,

  //String - Colour of the grid lines
  scaleGridLineColor : "rgba(0,0,0,.05)",

  //Number - Width of the grid lines
  scaleGridLineWidth : 1,

  //Boolean - Whether to show horizontal lines (except X axis)
  scaleShowHorizontalLines: true,

  //Boolean - Whether to show vertical lines (except Y axis)
  scaleShowVerticalLines: true,

  //Boolean - Whether the line is curved between points
  bezierCurve : true,

  //Number - Tension of the bezier curve between points
  bezierCurveTension : 0.4,

  //Boolean - Whether to show a dot for each point
  pointDot : true,

  //Number - Radius of each point dot in pixels
  pointDotRadius : 4,

  //Number - Pixel width of point dot stroke
  pointDotStrokeWidth : 1,

  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
  pointHitDetectionRadius : 20,

  //Boolean - Whether to show a stroke for datasets
  datasetStroke : true,

  //Number - Pixel width of dataset stroke
  datasetStrokeWidth : 2,

  //Boolean - Whether to fill the dataset with a colour
  datasetFill : true,

  //String - A legend template
  legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

},
  "barchart": {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      fillColor: "rgba(220,220,220,0.5)",
      strokeColor: "rgba(220,220,220,0.8)",
      highlightFill: "rgba(220,220,220,0.75)",
      highlightStroke: "rgba(220,220,220,1)",
      data: [65, 59, 80, 81, 56, 55, 40]
    },
    {
      label: "My Second dataset",
      fillColor: "rgba(151,187,205,0.5)",
      strokeColor: "rgba(151,187,205,0.8)",
      highlightFill: "rgba(151,187,205,0.75)",
      highlightStroke: "rgba(151,187,205,1)",
      data: [28, 48, 40, 19, 86, 27, 90]
    }
  ]
},
    "datatable": {
        "headers": [
            {
                "field": "sector",
                "label": "Sector"
            },
            {
                "field": "sqFtTaken",
                "label": "Sq ft taken",
                "type": "number"
            },
            {
                "field": "vsAvg",
                "label": "vs average",
                "modifier": "vsAvgChange",
                "type": "percentage"
            }
        ],
        "data": [
            {
                "sector": "Others",
                "sqFtTaken": 9110000,
                "vsAvg": 49,
                "vsAvgChange": "up"
            },
            {
                "sector": "Retail & Leisure",
                "sqFtTaken": 1810000,
                "vsAvg": 48,
                "vsAvgChange": "up"
            },
            {
                "sector": "Financial",
                "sqFtTaken": 610000,
                "vsAvg": 38,
                "vsAvgChange": "up"
            },
            {
                "sector": "Industry & Manufacturing",
                "sqFtTaken": 3310000,
                "vsAvg": 15,
                "vsAvgChange": "up"
            },
            {
                "sector": "Health & Education",
                "sqFtTaken": 5810000,
                "vsAvg": 1,
                "vsAvgChange": "up"
            },
            {
                "sector": "TMT",
                "sqFtTaken": 1110000,
                "vsAvg": -8,
                "vsAvgChange": "down"
            },
            {
                "sector": "Insurance",
                "sqFtTaken": 810000,
                "vsAvg": -9,
                "vsAvgChange": "down"
            },
            {
                "sector": "Associations",
                "sqFtTaken": 6310000,
                "vsAvg": -19,
                "vsAvgChange": "down"
            },
            {
                "sector": "Transportation",
                "sqFtTaken": 8210000,
                "vsAvg": -21,
                "vsAvgChange": "down"
            },
            {
                "sector": "Cultural",
                "sqFtTaken": 2510000,
                "vsAvg": -37,
                "vsAvgChange": "down"
            },
            {
                "sector": "Professional",
                "sqFtTaken": 310000,
                "vsAvg": -44,
                "vsAvgChange": "down"
            },
            {
                "sector": "Property",
                "sqFtTaken": 110000,
                "vsAvg": -52,
                "vsAvgChange": "down"
            },
            {
                "sector": "Government",
                "sqFtTaken": 3610000,
                "vsAvg": -56,
                "vsAvgChange": "down"
            },
            {
                "sector": "Utilities",
                "sqFtTaken": 7310000,
                "vsAvg": -61,
                "vsAvgChange": "down"
            },
            {
                "sector": "Services",
                "sqFtTaken": 3110000,
                "vsAvg": -72,
                "vsAvgChange": "down"
            },
            {
                "sector": "Construction",
                "sqFtTaken": 5510000,
                "vsAvg": -73,
                "vsAvgChange": "down"
            }
        ]
    },
     markers :[
        [
            51.590,
            -0.340
        ],
        [
            51.590,
            -0.0796
        ],
        [
            51.4470,
            -0.170
        ],
        [
            51.508,
            -0.376
        ]],
     points : [
        [
            51.590,
            -0.340
        ],
        [
            51.590,
            -0.0796
        ],
        [
            51.4470,
            -0.170
        ],
        [
            51.508,
            -0.376
        ]],
    excelData : [
        {
            "id": "1",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "2",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "3",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "4",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "5",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "6",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "7",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "8",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "9",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "10",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "11",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "12",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "13",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "14",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "15",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "16",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "17",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "18",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "down",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "19",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        },
        {
            "id": "20",
            "dealDate": "2015-02-21",
            "dealType": "investment sale",
            "propertyName": "110 High Holborn",
            "propertyLink": "/dashboard/property/110-High-Holborn",
            "size": 30,
            "price": 34,
            "priceChange": "up",
            "yield": 2.4,
            "yieldType": "",
            "purchaser": "Zolavo",
            "purchasersAgent": "DTZ",
            "vendor": "UBS Global",
            "vendorsAgent": "Savills"
        }
    ]


}